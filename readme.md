# Objetivo
El objetivo de este documento es el de plasmar el análisis, diseño y construcción de la práctica / proyecto asociado a la 5ª edición del curso TechU Partitioner.

El proyecto consta de dos partes principales; Backend y Frontal

# Backend
Desarrollado en nodejs.

## Uso
Para iniciar la parte backend únicamente debe situarse en la carpeta `back` y ejecutar el comando `npm start`.

## Tests
Bajo la carpeta `backend/tests`podrá encontrar los test del proyecto.

Para ejecutarlos, situese en la carpeta `back` y ejecute el comando `npm test`.

## Despliegue como contenedor usando docker
El módulo backend puede ser también ejecutado sobre un contenedor `docker`. El fichero `back/Dockerfile` contiene las instrucciones básicas para construir el contenedor.

Construcción del contenedor (situándose  `sudo docker build -t [[nombre_imagen]] [ruta fichero Dockerfile]]`.

A continuación, sólo tienes que ejecutar la imagen con `docker`

## Logs
Por defecto, los logs del sistema se almacenan en la carpeta `back/logs/traza.*`

# Frontal
El frontal ha sido desarrollado utilizando HTML5 y Polymer.

## Uso
Para iniciar la parte frontal únicamente debe situarse en la carpeta `front` y ejecutar el comando `polymer serve`.

A continuación abra su navegador chorme / chromium e introduzca la siguiente url; `http://localhost:3000`
