var mocha = require('mocha');
var chai = require('chai');
var chaihttp = require('chai-http');

chai.use(chaihttp);


// chai - assert, expect, should

var should = chai.should();
const expect = chai.expect;


// ** Autocontener el test unitario **
// mínimo tendremos que instanciar el Servidor
var server = require('../server');
//server. pruebaFuncionenServerJS();

url = "http://localhost:3000";
grantingAPI = "/grantingTickets/V02/";
usersAPI = "/users/V0/";
accountsAPI = "/accounts/V0/";
movementsAPI = "/accounts/V0/";
caasAPI = "";

mlabUrl = "https://api.mlab.com/api/1/databases/techu-5-project/collections";
mlabUsersCollection = "/users/";
mlabAccountsCollection = "/accounts/";
mlabMovementsCollection = "/movements/";
mlabApiKey = "apiKey=JfHW5FH_5fxHUyzKDobR-hIGpddEw3QW";

var _newUser = {
  first_name: "testUser",
  last_name: "testSurname",
  email: "test-email@fake-email.com",
  password: "miPassword",
  country: "ES"
};

var _createdUser;
var _tsec;


var _newAccount1 = {
        "personID" : "",
        "iban" : "1111 1111 1111 11111 11111111",
        "alias" : "cuenta test 1",
        "currency" : "€"
}

var _newAccount2 = {
        "personID" : "",
        "iban" : "2222 2222 2222 22222 22222222",
        "alias" : "cuenta test 2",
        "currency" : "$"
}

var _newMovement = {
    "accountID": "",
    "description":"Movimiento de prueba",
    "ammount": 10,
	  "accountID_origin": "",
	  "accountID_destination": ""
};

var _createdAccount1;
var _createdAccount2;

var _userstoDelete;
describe('Delete all test data: ',()=>{

  it('Delete test user', (done) => {
    chai.request(url)
      .get(usersAPI)
      .end( function(err,res){
        //	console.log(res.body);
        expect(res).to.have.status(200);

        var usuarios = res.body;
        // borramos todos los usuarios de test
        for(i=0 ; i<usuarios.length ; i++) {
          id = usuarios[i].personID;

          if(usuarios[i].first_name == _newUser.first_name) {
            chai.request(mlabUrl)
              .del(mlabUsersCollection + id + '?' + mlabApiKey)
              .end( function(err,res){
                expect(res).to.have.status(200);
                done();
              });
            }
        }
        //
      });
  });

  it('Delete all test accounts', (done) => {
    chai.request(mlabUrl)
      .get(mlabAccountsCollection + '?' + mlabApiKey)
      .end( function(err,res){
        //	console.log(res.body);
        expect(res).to.have.status(200);

        var accounts = res.body;
        // borramos todos los usuarios de test
        for(i=0 ; i<accounts.length ; i++) {
          id = accounts[i]._id.$oid;

          if(accounts[i].alias == _newAccount1.alias
           || accounts[i].alias == _newAccount2.alias) {
            //console.log(mlabUrl + mlabAccountsCollection + id + '?' + mlabApiKey);
            chai.request(mlabUrl)
              .del(mlabAccountsCollection + id + '?' + mlabApiKey)
              .end( function(err,res){
                expect(res).to.have.status(200);
                done();
              });
            }
          }
          //
      });
  });

  it('Delete all test movements', (done) => {
    chai.request(mlabUrl)
      .get(mlabMovementsCollection + '?' + mlabApiKey)
      .end( function(err,res){
        //	console.log(res.body);
        expect(res).to.have.status(200);

        var movements = res.body;
        // borramos todos los movements de test
        for(i=0 ; i<movements.length ; i++) {
          id = movements[i]._id.$oid;

          if(movements[i].description == _newMovement.description
           || movements[i].description == _newMovement.description) {
            //console.log(mlabUrl + mlabAccountsCollection + id + '?' + mlabApiKey);
            chai.request(mlabUrl)
              .del(mlabMovementsCollection + id + '?' + mlabApiKey)
              .end( function(err,res){
                expect(res).to.have.status(200);
                done();
              });
            }
          }
          //
      });
  });

});

describe('Test : Create user and login: ',()=>{

  it('Create a test user and login', (done) => {
		chai.request(url)
			.post(usersAPI)
      .send(_newUser)
			.end( function(err,res){
        expect(res).to.have.status(200);

        _createdUser = res.body[0];
        expect(_createdUser).to.have.property('personID');
        expect(_createdUser).to.have.property('first_name').to.be.equal(_newUser.first_name);
        expect(_createdUser).to.have.property('email').to.be.equal(_newUser.email);
        console.log("\t Usuario; " + _createdUser.personID + ', ' + _createdUser.email);

        var grantingBody = {
            "authentication": {
              "consumerID": "00000001",
              "authenticationData": [{
                "authenticationData": [ _newUser.password ],
                "idAuthenticationData": "password"
              }],
              "authenticationType": "02",
              "userID": _createdUser.email
            }
          };
        // Perform login
    		chai.request(url)
    			.post(grantingAPI)
          .send(grantingBody)
    			.end( function(err,res){
            expect(res).to.have.status(200);

            body = res.body;

            expect(res.headers.tsec).not.equal("");
            _tsec = res.headers.tsec;

            done();
    			 });
			});
	});

  it('Bad login', (done) => {
    var grantingBody = {
        "authentication": {
          "consumerID": "00000001",
          "authenticationData": [{
            "authenticationData": [ "password incorrecta" ],
            "idAuthenticationData": "password"
          }],
          "authenticationType": "02",
          "userID": _createdUser.email
        }
      };
		chai.request(url)
    .post(grantingAPI)
    .send(grantingBody)
    .end( function(err,res){
        expect(res).to.have.status(403);
        done();
			});
	});
});



describe('Test: Accounts: ',()=>{

	it('Insert account' , (done) => {
    // prepare _newAccount1 and 2 data
    _newAccount1.personID = _createdUser.personID;
    console.log("\t Usuario; " + _newAccount1.personID);

    chai.request(url)
      .post(accountsAPI)
      .set('tsec', _tsec )
      .send(_newAccount1)
      .end( function(err,res){

      console.log("\t Cuenta; " + res.body[0].accountID + ", iban; " + res.body[0].iban + ", usuario; " + res.body[0].personID);

      expect(res).to.have.status(200);
      expect(res.headers.tsec).not.equal("");
      expect(_newAccount1.iban).equal(res.body[0].iban);
      _createdAccount1 = res.body[0];
      _tsec = res.headers.tsec;
console.log(_createdAccount1.accountID);
      done();
     });
  });

  it('Insert account 2' , (done) => {
    // prepare _newAccount1 and 2 data
    _newAccount2.personID = _createdUser.personID;
    console.log("\t Usuario; " + _newAccount2.personID);

    chai.request(url)
      .post(accountsAPI)
      .set('tsec', _tsec )
      .send(_newAccount2)
      .end( function(err,res){

      console.log("\t Cuenta; " + res.body[0].accountID + ", iban; " + res.body[0].iban + ", usuario; " + res.body[0].personID);

      expect(res).to.have.status(200);
      expect(res.headers.tsec).not.equal("");
      expect(_newAccount2.iban).equal(res.body[0].iban);
      _createdAccount2 = res.body[0];
      _tsec = res.headers.tsec;

      done();
     });
  });

  it('Get Accounts => expect 2', (done) => {
    console.log("\t Usuario; " + _createdUser.personID + ', ' + _createdUser.email);

    chai.request(url)
      .get(accountsAPI)
      .set('tsec', _tsec )
      .end( function(err,res){
        expect(res).to.have.status(200);
        expect(res.body.length).to.equal(2);
        expect(res.headers.tsec).not.equal("");
        _tsec = res.headers.tsec;

        console.log("\t Usuario; " + _createdUser.personID + ', ' + _createdUser.email + " Nº accounts; " + res.body.length );

        done();
       });
  });

});

describe('Test: Accounts / Movements & Transfers: ',()=>{

	it('Insert movement; Ingreso' , (done) => {
    // prepare _newAccount1 and 2 data
    //_newAccount1.personID = _createdUser.personID;
    _newAccount1.accountID = _createdAccount1.accountID;

    console.log("\t Usuario; " + _newAccount1.personID + ", cuenta; " + _newAccount1.accountID);

    //_newMovement.accountID = _newAccount1.accountID;
    _newMovement.ammount = 100;
    _newMovement.accountID_origin = "donante anónimo";
    _newMovement.accountID_destination = _newAccount1.accountID;

    chai.request(url)
      .post(movementsAPI + _newAccount1.accountID + "/movements/")
      .set('tsec', _tsec )
      .send(_newMovement)
      .end( function(err,res){
console.log(res.body);
      console.log("\t Movimiento; " + res.body[0].movementID + ", saldo anterior; " + res.body[0].previousBalance + " saldo posterior; " + res.body[0].newBalance);

      expect(res).to.have.status(200);
      expect(res.headers.tsec).not.equal("");
      //expect(_newAccount1.iban).equal(res.body[0].iban);
      //_createdAccount1 = res.body[0];
      _tsec = res.headers.tsec;

      done();
     });
  });
});

/*
describe('Delete all test data: ',()=>{

  it('Delete test user', (done) => {
    chai.request(url)
      .get(usersAPI)
      .end( function(err,res){
        //	console.log(res.body);
        expect(res).to.have.status(200);

        var usuarios = res.body;
        // borramos todos los usuarios de test
        for(i=0 ; i<usuarios.length ; i++) {
          id = usuarios[i].personID;

          if(usuarios[i].first_name == _newUser.first_name) {
            chai.request(mlabUrl)
              .del(mlabUsersCollection + id + '?' + mlabApiKey)
              .end( function(err,res){
                expect(res).to.have.status(200);
                console.log(usuarios[i].first_name);
                done();
              });
            }
        }
        //
      });
  });

  it('Delete all test accounts', (done) => {
    chai.request(mlabUrl)
      .get(mlabAccountsCollection + '?' + mlabApiKey)
      .end( function(err,res){
        //	console.log(res.body);
        expect(res).to.have.status(200);

        var accounts = res.body;
        // borramos todos los usuarios de test
        for(i=0 ; i<accounts.length ; i++) {
          id = accounts[i]._id.$oid;

          if(accounts[i].alias == _newAccount1.alias
           || accounts[i].alias == _newAccount2.alias) {
            //console.log(mlabUrl + mlabAccountsCollection + id + '?' + mlabApiKey);

            chai.request(mlabUrl)
              .del(mlabAccountsCollection + id + '?' + mlabApiKey)
              .end( function(err,res){
                expect(res).to.have.status(200);
                done();
              });
            }
          }
          //
      });
  });
});
*/
