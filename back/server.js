console.log("Inicializando server.js");


// Inicializando módulo de configuración (coge los ficheros alojados en directorio config)
var config = require('config');
// ejemplo de acceso
//console.log(config.get('mlab.apiKey'));


// Inicialización de módulo de logs (log4js) - https://www.npmjs.com/package/log4js
var log4js = require('log4js');


log4js.configure({
  appenders: {
    everything: { type: 'file', filename: './logs/traza.log', maxLogSize: 10485760, backups: 3, compress: false }
  },
  categories: {
    default: { appenders: [ 'everything' ], level: 'debug'}
  }
});

var logger = log4js.getLogger();


// Configuración de módulos
// ToDo: Ajustar configuración de logs (lectura de configuración de fichero, escritura en fichero, etc)
logger.level = 'debug';

logger.info("*********************************************");
logger.info("    Iniciando servidor");
logger.info("*********************************************");
logger.info("Inicializando módulos de terceros");
logger.debug("log4js - Módulo inicializado y configurado");

/*  Inicialización y configuración de módulo express
  - https://www.npmjs.com/package/express

  Variables;
    - app
    - port
    - request-json
    - bodyParser
    - path
*/
logger.debug("express - Inicializando módulo");
var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

//variable para que se pueda parsear el req.body y no salga como undefined
var bodyParser = require('body-parser');

// for parsing application/json
app.use(bodyParser.json());
var path = require('path');
app.listen(port);



//variable para poder usar el request-json
var requestjson = require('request-json');
var cors = require('cors');





logger.debug("express - Módulo inicializado y configurado");

/*
https://www.npmjs.com/package/request-json
//variable para poder usar el request-json
var requestjson = require('request-json');
*/
logger.info("Inicializando módulos de terceros");

logger.info("Inicilaizando módulo jsonwebtoken");
var jwt = require('jsonwebtoken');
//var jwtBlacklist = require('express-jwt-blacklist');

  const jwtExpiresIn = config.get('configuration.jsonwebtoken.expiresIn');
  const jwtAlgorithm = config.get('configuration.jsonwebtoken.algorithm');
  const jwtSecret = config.get('configuration.jsonwebtoken.secret');

    const jwtConfiguration = {
      "algorithm" : jwtAlgorithm,
      "expiresIn": jwtExpiresIn
    };
logger.info("Módulo jsonwebtoken inicializado. Algoritmo; " + jwtConfiguration);


logger.info("Inicializando módulos propios");

var logUtils = require('./techu_node_modules/techu_logs/module_logs.js');
logger.debug("Módulo de utilidades de logs inicializado");

var caas = require('./techu_node_modules/techu_caas/module_caas.js');
caas.setLogger(logger);
caas.setConfig( config.get('configuration') );
logger.debug("Módulo caas (Content as a Service) inicializado");


var users = require('./techu_node_modules/techu_users/module_users.js');
users.setLogger(logger);
users.setConfig( config.get('configuration') );
logger.debug("Módulo Users inicializado");

var accounts = require('./techu_node_modules/techu_accounts/module_accounts.js');
accounts.setLogger(logger);
accounts.setConfig( config.get('configuration') );
logger.debug("Módulo Accounts inicializado");

var serviceCheck = require('./techu_node_modules/techu_service_check/module_service_check.js');
serviceCheck.setLogger(logger);
// ToDo: set para BBDD + set configuración versión del servicio, etc
logger.debug("Módulo Service Check (Etherification) inicializado");


logger.info("Módulos propios inicializados");



logger.info("Servidor de nodejs escuchando en puerto " + port + " con pid ", process.pid);

/*
  Intercepta todas las peticiones http e inyecta las cabeceras necesarias para
  eliminar las restricciones de CORS
*/
app.all('*', cors(), function(req, res, next){

  logUtils.logRequestToAPI(logger, req);

  // La cabecera "Access-Control-Expose-Headers" define las cabeceras que pone a disposición el Navegador.
  // Sin estas no se puede recuperar el tsec en el navegador utilizando data.detail.xhr.getResponseHeader("tsec")
  res.setHeader("Access-Control-Expose-Headers", "Access-Control-*, Content-Length, Content-Encoding, Accept-Ranges, Content-Range, Access-Control-Allow-Origin, tsec");
  res.setHeader("Access-Control-Allow-Headers", "Access-Control-*, Origin, X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept, tsec");
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS, HEAD');
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Credentials', false);
  res.setHeader('Access-Control-Max-Age', '86400');

  res.setHeader('Allow', 'GET, POST, PUT, DELETE, OPTIONS, HEAD');

  next();
});

/*
  Control de acceso a los servicios. Verifica el tsec y regenera

  Ojo. elimina ciertos paths del control de acceso (ej. grantingTickets ) en base a una lista blanca de servicios

  Si todo va bien, fija el nuevo tsec en la response, fija el usuario en res.locals (sólo disponible en servidor)
  y hace un next() para que siga con el siguiente patrón de url

  Si no va bien devuelve un error http y mensaje descriptivo
*/
app.use('/', function(req, res, next){
  logger.debug("Módulo de control de acceso");
  logger.debug('Request Type:', req.method);
  logger.debug(req);
    var publicServices = [
      "/grantingTickets/", "/caas/", "/_service/",
      "/users/V0"
    ];
    // intentamos encontrar el patrón en los publicServices para ver si es un servicio público
    var url = req.originalUrl;
    var whiteList = false;
    for (i=0 ; i< publicServices.length ; i++) {
    	if ( url.indexOf( publicServices[i] ) >=0 ) {
        whiteList = true;
        break;
      }
    }
    logger.debug("Patrón encontrado dentro de la lista blanca (público)??? " + whiteList);
    // la url del servicio / el servicio debe estar protegido ==> necesita tsec
    if ( !whiteList ) {
      var tsec = req.headers.tsec;
      if(tsec != undefined) {
        logger.debug("Tsec recibido, validando");
        try {
          var decoded = jwt.verify(tsec, jwtSecret, jwtConfiguration);
          logger.debug("Tsec válido");
          logger.debug(decoded);

          delete decoded.iat;
          delete decoded.exp;

          // fija los datos del tsec en el res.locals.decodedTsec ==> para resto de módulos
          res.locals.decodedTsec = _formatUser(decoded);
          logger.debug("Se fija la info del usuario logado en res.locals.decodedTsec;");
          logger.debug(res.locals.decodedTsec);

          // Regeneramos un nuevo tsec a partir de los datos del anterior
          // Habría que optimizar este algoritmo para temas de rendimiento y por seguridad
          // ya que genera n tsec's válidos. Sólo debería regenerarlos cuando estén a punto de caducar
          logger.debug("Regenerando un nuevo tsec a partir del antigio");
          var tsec = jwt.sign(decoded, jwtSecret, jwtConfiguration);

          // fijamos el nuevo tsec en cabecera en la respuesta de la petición
          res.setHeader("tsec", tsec );
          // fijamos la info del usuario en base a la información del Tsec
          // esta información será utilizada para realizar funciones de control de acceso sobre diferentes servicios
          // ej. que el usuario esté accediendo a una cuenta suya
          // la estructura y datos debería ser algo como
          // { userID: 'tkearney1@cloudflare.com', authenticationType: '02', consumerID: '00000001' }
          res.locals.usuario = decoded;

          logger.debug("Parámetro tsec fijadon en las cabeceras de respuesta y se continúa la ejecución");
          // tsec validado y regenerado + info usuario añadida ==> next()
          next();
        } catch(err) { // ok cuando no es válido
          logger.debug("Error descifrando tsec: ");
          logger.debug(err);

          res.status(403);
          respuesta = "OMG !!!, authentication ERRR, please provide a valid tsec. \n"
            + err.name + ", " + err.message;
          res.send(respuesta);
        }
      } else {
        logger.debug("Error, no se ha recibido el parámetro tsec");
        res.status(403);
        respuesta = "OMG !!!, authentication ERRR, need a tsec";
        res.send(respuesta);
      }
    } else {
      logger.debug("El servicio invocado está dentro de la lista blanca (no necesita tsec) => se continúa la ejecución");
      // El servicio invocado está dentro de la lista blanca (no necesita tsec) => next
      next();
    }
});

// función que genera la info de un usuario a partir del modelo
function _formatUser (user) {
  // datos a devolver
  return {
    "authenticationData": [],
    "authenticationState": "OK",
    "user": {
        "id": user.userID,
        "authenticationType": user.authenticationType,
        "alias": "none",
        "role": "O",
        "isAdmin" : user.isAdmin,
        "otherUserInfo": {
            "MEDICOS_BARCELONA": "N",
            "FILE_OPERATION": "N",
            "ORIGIN_CX": "N",
            "ORIGIN_UNNIM": "N"
        },
        "personalization": {
            "channelCode": "04",
            "personalizationCode": "T"
        },
        "person": {
            //"id": dataInTsec.personID
            "id": user.personID
        }
    }
  };
}

/**
  Método para hacer login del usuario. Genera un tsec que será necesario utilizar
  en las llamadas al resto de servicios

  Ejemplo.
  POST http://localhost:3000/grantingTickets/V02
  {
    "authentication": {
      "consumerID": "00000001",
      "authenticationData": [{
        "authenticationData": ["12345"],
        "idAuthenticationData": "password"
      }],
      "authenticationType": "02",
      "userID": "tkearney1@cloudflare.com"
    }
  }
*/
app.post('/grantingTickets/V02',
  function (req, res, next) {

    var user = {
      "userID": req.body.authentication.userID,
      "password": req.body.authentication.authenticationData[0].authenticationData,
      "authenticationType": req.body.authentication.authenticationType,
      "consumerID": req.body.authentication.consumerID
    }
    logger.debug ("Intento de login de usuario: " + user.usuario + ", authenticationType: " +
      user.authenticationType + "aap (aka consumerID): " + user.aapOrigin);
    res.locals.user = user;

    users.getUserAndValidate(user, res, next);
  }, function(req, res) {
    resultado = res.locals.resultado;
    logger.debug("Petición procesada, devolviendo datos");

    var respuesta = "";
    if ( (resultado == undefined ) || (resultado.length == 0) ){
      logger.debug("Login incorrecto");
      res.status(403);
      respuesta = "OMG !!!, Unauthorized - Incorrect Auth information";
    } else { // login correcto, se genera tsec y devuelven datos del usuario
      logger.debug("Login correcto, generando jwt");

      var dataInTsec = res.locals.user;
      delete dataInTsec.password;
      dataInTsec.personID = resultado[0]._id.$oid;
      dataInTsec.isAdmin = false;

      // datos a devolver
      respuesta = _formatUser(dataInTsec);

      logger.debug(tsec);
      // Generación de token
      var tsec = jwt.sign(dataInTsec, jwtSecret, jwtConfiguration);

      res.setHeader("tsec", tsec );
      res.status(200);
    }
    res.send(respuesta);
  }
);

/* Devuelve el listado de tenants de imágenes / assets
 * Ej. Petición: http://localhost:3000/caas/v0/tenants
*/
app.get('/caas/V0/tenants/',
  function(req, res, next) {
    caas.getTenants(res, next);
  }, function(req, res) {
    logger.debug("Petición procesada, devolviendo datos");
    resultado = res.locals.resultado;
    if ( resultado == undefined || resultado.length == 0){
      res.status(500);
      resultado = "OMG !!!, Server error";
    }
    res.send(resultado);
  }
);

/* Devuelve el listado de images de un tentant
 * Ej. Petición: http://localhost:3000/caas/v0/tenants/1/images
*/
app.get('/caas/V0/tenants/:tenantId/',
  function(req, res, next) {
    var tenant = req.params.tenantId;

    caas.getImagesFromTenant(tenant, res, next);
  }, function(req, res) {
    logger.debug("Petición procesada, devolviendo datos");
    resultado = res.locals.resultado;
    if ( resultado == undefined || resultado.legth == 0){
      res.status(500);
      resultado = "OMG !!!, Server error";
    }
    res.send(resultado);
  }
);

/* Devuelve la información de un asset concreto
 * Ej. Petición: http://localhost:3000/caas/v0/tenants/1/images/222
*/
app.get('/caas/V0/tenants/:tenantId/images/:imageId',
  function(req, res, next) {
    var tenant = req.params.tenantId;
    var image = req.params.imageId;

    caas.getImageFromTenant(tenant, image, res, next);
  }, function(req, res) {
    logger.debug("Petición procesada, devolviendo datos");
    resultado = res.locals.resultado;
    if ( resultado == undefined || resultado.legth == 0){
      res.status(500);
      resultado = "OMG !!!, Server error";
    }
    res.send(resultado);
  }
);

/*
Método de información relativa a la información básica del serviico

Definido según patrón definido en
  "Ether Technical Model - Services Interface Model.pub" / The /_service endpoint

  * Ej. Petición: http://localhost:3000/_service/info
*/
app.get('/_service/info',
  function(req, res) {

    res.send( serviceCheck.getServiceInfo(req, res) );
  }
);

/*
Método de información relativa al health check del servicio
  - 200 - ok
  - 503 - Service unavailable

Ej. Petición - http://localhost:3000/_service/health
*/
app.get('/_service/health',
  function(req, res) {

    if ( "OK" == serviceCheck.getServiceHealth() ) {
      res.status(200).send('OK');
    } else {
      res.status(503).send('Service Unavailable');
    }
  }
);

/* Devuelve el listado de usuarios
 * Ej. Petición: localhost:3000/users/V0/
*/
app.get('/users/V0/',
  function(req, res, next) {

    users.getUsers(res, next);
  }, function(req, res) {
    logger.debug("Petición procesada, devolviendo datos");
    //res.locals.resultado[0].personID = res.locals.resultado[0]._id.$oid;
    //delete res.locals.resultado[0]._id;
    resultado = res.locals.resultado;
    if ( resultado == undefined || resultado.legth == 0){
      res.status(500);
      resultado = "OMG !!!, Server error";
    }
    res.send(resultado);
  }
);

/* Devuelve la información de un usuario concreto
 * Ej. Petición: http://localhost:3000/caas/v0/tenants/1/images
*/
app.get('/users/V0/:personID/',
  function(req, res, next) {
    var id = req.params.personID;

    users.getUser(id, res, next);
  }, function(req, res) {
    logger.debug("Petición procesada, devolviendo datos");
    //res.locals.resultado[0].personID = res.locals.resultado[0]._id.$oid;
    //delete res.locals.resultado[0]._id;
    resultado = res.locals.resultado;
    //resultado = _formatUser(res.locals.resultado[0]);
    if ( resultado == undefined || resultado.legth == 0){
      res.status(500);
      resultado = "OMG !!!, Server error";
    }
    res.send(resultado);
  }
);

app.get('/users/V1/info/',
  function(req, res, next) {
    logger.debug("Se va a consultar las cuentas (accounts) del usuario logado");
    var personID = res.locals.decodedTsec.user.person.id;
    logger.debug("Usuario con personID; " + personID);

    users.getUser(personID, res, next);
  }, function(req, res) {
    logger.debug("Petición procesada, devolviendo datos");
    //res.locals.resultado[0].personID = res.locals.resultado[0]._id.$oid;
    //delete res.locals.resultado[0]._id;
    resultado = res.locals.resultado;
    //resultado = _formatUser(res.locals.resultado[0]);
    if ( resultado == undefined || resultado.legth == 0){
      res.status(500);
      resultado = "OMG !!!, Server error";
    }
    res.send(resultado);
  }
);


app.delete('/users/V1/',
  function(req, res, next) {
    var personID = res.locals.decodedTsec.user.person.id;
    logger.debug("Se va a eliminar el usuario; " + personID);

    users.deleteUser(personID, res, next);
  }, function(req, res) {
    if( resultado == undefined ){
      logger.debug("Error al dar de baja el usuario");
      res.status(500);
      res.send("Error al dar de baja el usuario; " + personId);
    }else {
      logger.debug("Usuario dado de baja");
      res.send( { "message": "Usuario dado de baja"} ) ;
    }
  }
);

/* Inserta un nuevo usuario en la base de datos
 localhost:3000//users/V0/
 {
      "first_name" : "miNombre",
      "last_name" : "apellidos",
      "email" : "mi email",
      "password" : "ppppppppp22222",
      "country" : "ES"
}
*/
app.post("/users/V0/",
  function(req, res, next) {
    // capturamos los datos de entrada para el alta de usuario

    var newUser = {
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "email" : req.body.email,
      "password" : req.body.password,
      "country" : req.body.country
    };

    logger.debug ("Datos introducidos; " +  newUser.first_name + ", " +
      newUser.last_name + ", " +
      newUser.email + ", " +
      newUser.country);

    users.newUser(newUser, res, next);
  }, function(req, res) {
    logger.debug("Petición procesada, devolviendo datos");
    //res.locals.resultado[0].personID = res.locals.resultado[0]._id.$oid;
    //delete res.locals.resultado[0]._id;
    resultado = res.locals.resultado;
    if ( resultado == undefined || resultado.legth == 0){
      res.status(500);
      resultado = "OMG !!!, Server error al dar de alta usuario";
    }
    res.send(resultado);
  }
);


/* Devuelve la lista de las cuentas de un usuario
    No hace falta pasar el personID ya que lo saca del tsec
 * Ej. Petición: http://localhost:3000/users/V0/accounts
*/
app.get('/accounts/V0/',
  function(req, res, next) {
    logger.debug("Se va a consultar las cuentas (accounts) del usuario logado");
    var personID = res.locals.decodedTsec.user.person.id;
    logger.debug("Usuario con personID; " + personID);

    accounts.getAccounts(personID, res, next);
  },function(req, res) {
    logger.debug("Petición procesada, devolviendo datos");
    //res.locals.resultado[0].personID = res.locals.resultado[0]._id.$oid;
    //delete res.locals.resultado[0]._id;
    resultado = res.locals.resultado;
    if ( resultado == undefined || resultado.legth == 0){
      res.status(500);
      resultado = "OMG !!!, Server error";
    }

    res.send(resultado);
  }
);


/* Inserta una nueva cuenta en la base de datos
 localhost:3000//users/V0/

*/
app.post("/accounts/V0/",
  function(req, res, next) {
    // capturamos los datos de entrada para el alta de usuario

    var personID = res.locals.decodedTsec.user.person.id;
    logger.debug("Usuario con personID; " + personID);

    var newAccount = {
      "personID" : personID,
      "iban" : req.body.iban,
      "alias" : req.body.alias,
      "currency" : req.body.currency
    };

    logger.debug ("Datos introducidos; " +  newAccount.iban + ", " +
      newAccount.alias + ", " +
      newAccount.currency + ", ");

    accounts.newAccount(newAccount, res, next);
  }, function(req, res) {
    logger.debug("Petición procesada, devolviendo datos");
    //res.locals.resultado[0].personID = res.locals.resultado[0]._id.$oid;
    //delete res.locals.resultado[0]._id;
    resultado = res.locals.resultado;
    if ( resultado == undefined || resultado.legth == 0){
      res.status(500);
      resultado = "OMG !!!, Server error al dar de alta usuario";
    }
    res.send(resultado);
  }
);

/* Inserta una nueva cuenta en la base de datos
 localhost:3000//users/V0/

*/
app.delete("/accounts/V0/:accountID",
  function(req, res, next) {
    // capturamos los datos de entrada para el alta de usuari
    var personID = res.locals.decodedTsec.user.person.id;
    var accountID = req.params.accountID;
    logger.debug("Usuario con personID; " + personID + ", Borrar cuenta; " + accountID);

    var deleteAccount = {
      "personID" : personID,
      "accountID" : accountID
    };

    accounts.deleteAccount(deleteAccount, res, next);
  }, function(req, res) {
    logger.debug("Petición procesada, devolviendo datos");
    //res.locals.resultado[0].personID = res.locals.resultado[0]._id.$oid;
    //delete res.locals.resultado[0]._id;
    resultado = res.locals.resultado;
    if ( resultado == undefined || resultado.legth == 0){
      res.status(500);
      resultado = "OMG !!!, Server error al dar de delete cuenta";
    }
    res.send(resultado);
  }
);

/*
  Devuelve el balance de una cuenta
  Ej. localhost:3000/accounts/V0/5afc38c3e5e23f0c96672ad9/balance
*/
app.get('/accounts/V0/:accountID/balance',
  function(req, res, next) {
    // Recuperamos el balance de la cuenta

    var accountID = req.params.accountID;
    var personID = res.locals.decodedTsec.user.person.id;
    logger.debug("Usuario con personID; " + personID + ", cuenta; " + accountID);

    logger.debug("Se van a consultar el balance de la cuenta");

    accounts.getAccountBalance(personID, accountID, res, next);
  }, function(req, res) {
    logger.debug("Petición procesada, devolviendo datos");

    resultado = res.locals.balance;
    if ( resultado == undefined ){
      res.status(500);
      resultado = "OMG !!!, Server error";
    }
    res.send(JSON.stringify(resultado));
  }
);

/* Devuelve la lista de las cuentas de un usuario
    No hace falta pasar el personID ya que lo saca del tsec
 * Ej. Petición: http://localhost:3000/users/V0/accounts
*/
app.get('/accounts/V0/:accountID/movements/',
  function(req, res, next) {

    var accountID = req.params.accountID;
    var personID = res.locals.decodedTsec.user.person.id;
    logger.debug("Usuario con personID; " + personID + ", cuenta; " + accountID);

    logger.debug("Se van a consultar los movimientos de las cuenta");

    accounts.getMovements(personID, accountID, res, next);
  }, function(req, res) {
    logger.debug("Petición procesada, devolviendo datos");
    //res.locals.resultado[0].personID = res.locals.resultado[0]._id.$oid;
    //delete res.locals.resultado[0]._id;
    resultado = res.locals.resultado;
    if ( resultado == undefined || resultado.legth == 0){
      res.status(500);
      resultado = "OMG !!!, Server error";
    }
    res.send(resultado);
  }
);

/*
  Añade un nuevo movimiento a la cuenta

  Sirve para simular un abono o reintegro
  hay que pasarle los identificadores de cuentas

  /accounts/V0/:accountID/movements/
  {
    "accountID": "5afc38c3e5e23f0c96672ad9",
    "description":"movimiento de prueba",
    "ammount": 10000000,
	"iban_origin": "account id origen",
	"iban_destination": "5afc38c3e5e23f0c96672ad9"
}
*/
app.post('/accounts/V0/:accountID/movements/',
  function(req, res, next) {
    // capturamos los datos de entrada para el alta de movimiento
    var accountID = req.params.accountID;
    var personID = res.locals.decodedTsec.user.person.id;
    logger.debug("Usuario con personID; " + personID + ", accountID; " + accountID);

    var newMovement = {
      "accountID" : accountID,
      "description": req.body.description,
      "ammount" : req.body.ammount,
      "date" : new Date(Date.now()).toLocaleString(),
      "iban_origin" : req.body.iban_origin,
      "iban_destination" : req.body.iban_destination,
      "tipo" : req.body.tipo
    };

    logger.debug ("Datos introducidos; ");
    logger.debug(newMovement);

    accounts.newMovement(newMovement, res, next);
  }, function(req, res) {
    logger.debug("Petición procesada, devolviendo datos");
    resultado = res.locals.resultado;
    if ( resultado == undefined || resultado.legth == 0){
      res.status(500);
      resultado = "OMG !!!, Server error al dar de alta usuario";
    }
    res.send(resultado);
  }
);


/*
  Realiza una trasferencia (entre cuentas del propio banco o de terceros)

  Si la trasferencia es del propio banco entonces añade la cantidad obtenida
*/
app.post('/transfer/V0/',
  function(req, res, next) {
    // Para hacer la trasferencia primero quitamos la cantidad (ammount) de la cuenta origen
    var accountID = req.body.accountID;
    var iban_destination = req.body.iban_destination
    var personID = res.locals.decodedTsec.user.person.id;
    logger.debug("Usuario con personID; " + personID) + ", accountID; " + accountID;

    var newMovement = {
      "accountID" : accountID,
      "description": req.body.description,
      "ammount" : -req.body.ammount,
      "date" : new Date(Date.now()).toLocaleString(),
      "iban_origin" : req.body.iban_origin,
      "iban_destination" : req.body.iban_destination,
      "tipo" : req.body.tipo
    };

    logger.debug ("Datos introducidos; ");
    logger.debug(newMovement);

    accounts.newMovement(newMovement, res, next);
  }, function(req, res, next) {
      // Después localizamos la cuenta (el accountID_destination) correlacionando con el parámetro iban_destination
      // el accountID_destination se almacenará en el res.locals.resultado
      logger.debug("Intentando localizar el iban; " + req.body.iban_destination);
      var personID = res.locals.decodedTsec.user.person.id;

      accounts.getAccount( undefined, personID, req.body.iban_destination, res, next);
  }, function(req, res, next) {
      // res.resultado están los datos de la cuenta (si es que se ha encontrado, si no se ha hecho es que pertenece a otro banco)
      resultado = res.locals.resultado;

      // la cuenta pertenece a otro banco
      if ( resultado == undefined || resultado.legth == 0){
        // si pertenece a otro banco entonces devolvemos la info de la transacción y ya está
        logger.debug("La cuenta pertenece a otro banco");

        var newMovement = {
          "accountID" : req.body.accountID,
          "description": req.body.description,
          "ammount" : req.body.ammount,
          "date" : new Date(Date.now()).toLocaleString(),
          "iban_origin" : req.body.iban_origin,
          "iban_destination" : req.body.iban_destination,
          "tipo" : req.body.tipo
        };
        // devolvemos los datos de la transferencia (movimiento) ejecutados
        res.locals.resultado = newMovement;
        next();
      } else { // la cuenta pertence a nuestro banco ==> restamos la pasta, hacemos el movimiento inverso en esa cuenta
        // la cuenta pertenece a nuestro banco
        console.log("La cuenta pertenece a nuestro banco ... se va a hacer el abono");
        var accountID = resultado[0].accountID;

        var iban_destination = req.body.iban_destination
        var personID = res.locals.decodedTsec.user.person.id;
        logger.debug("Usuario con personID; " + personID) + ", accountID; " + accountID;

        var newMovement = {
          "accountID" : accountID,
          "description": req.body.description,
          "ammount" : req.body.ammount,
          "date" : new Date(Date.now()).toLocaleString(),
          "iban_origin" : req.body.iban_origin,
          "iban_destination" : req.body.iban_destination,
          "tipo" : req.body.tipo
        };

        logger.debug ("Datos de la trasfernecia (en cuenta destino); ");
        logger.debug(newMovement);

        accounts.newMovement(newMovement, res, next);
      }
    }, function(req, res) {
    logger.debug("Petición procesada, devolviendo datos");
    resultado = res.locals.resultado;
    if ( resultado == undefined || resultado.legth == 0){
      res.status(500);
      resultado = "OMG !!!, Server error al dar de alta usuario";
    }
    res.send(resultado);
  }
);

// jugando y probando el funcionamiento de funciones asíncronas y callbacks (next)
/*
app.get('/caas/v0/callbacks/',
  function(req, res, next) {
    console.log(typeof next);
    console.log("1");
    setTimeout( function () {
      console.log("1.1");

      setTimeout( function () {
        console.log("1.1.1");
        caas.pruebaAsync(res, next);
        //next();
      }, 2000);


    }, 1000);

  }, function(req, res) {
    console.log("2");
    res.send(res.locals.resultado);
  }
);
*/
