Polymer({

  is: 'cells-atom-video',

  properties: {
    /**
     * List of media resources objects for video.
     * Example:<pre>
     * [{"src": "urlVideo", "type": "mp4"}]
     * </pre>
     */
    sources: {
      type: Array
    },

    /**
     * Play video in a loop.
     */
    loop: {
      type: Boolean,
      value: false
    },

    /**
     * Mute video sound.
     */
    muted: {
      type: Boolean,
      value: false
    },

    /**
     * Play video automatically without user interaction.
     */
    autoplay: {
      type: Boolean,
      value: false
    },

    /**
     * Path to image shown as video fallback.
     */
    poster: {
      type: String
    },

    /**
     * Tracks to show over video. Time is setting in milliseconds.
     * Example:<pre>
     * [{"title": "Title", "text": "text", "start": 1000, "end": 4000}]
     * </pre>
     */
    tracks: {
      type: Array,
      observer: '_tracksObserver'
    },

    /**
     * Set true to play the video and false to pause it.
     */
    play: {
      type: Boolean,
      observer: '_playObserver'
    },

    _currentTrack: Object,

    _tracksHandlers: {
      type: Array,
      value: function() {
        return [];
      }
    }
  },

  listeners: {
    'video.play': '_videoPlayListener',
    'video.playing': '_setupTracks'
  },

  _videoPlayListener: function() {
    this.dispatchEvent(new CustomEvent('video-play-started', {
      bubbles: true,
      composed: true
    }));
  },

  _tracksObserver: function(newTacks, oldTracks) {
    this._unsetupTracks(oldTracks || []);
    this._currentTrack = null;

    if (newTacks && !this.$.video.paused) {
      this._setupTracks();
    }
  },

  _playObserver: function() {
    if (this.play) {
      this.$.video.play();
    } else {
      this.$.video.pause();
      this._unsetupTracks();
    }
  },

  _setupTracks: function() {
    if (this.tracks) {
      // To milliseconds
      var videoDuration = this.$.video.duration * 1000;
      var currentTime = this.$.video.currentTime;

      // Make sure that delete previous setup.
      this._unsetupTracks();

      this.tracks.forEach(function(track) {
        track.handlers = [];

        if (!track.end || track.end > videoDuration) {
          track.end = videoDuration;
        }

        if (track.start >= currentTime) {
          track.handlers.push(this.async(this._showTrack.bind(this, track), track.start - currentTime));
          track.handlers.push(this.async(this._hideTrack.bind(this, track), track.end - currentTime));
        }
      }, this);
    }
  },

  _unsetupTracks: function(tracks) {
    tracks = tracks || this.tracks || [];

    tracks.forEach(function(track) {
      if (track.handlers) {
        track.handlers.forEach(this.cancelAsync, this);
      }
    }, this);
  },

  _showTrack: function(track) {
    this.$.track.classList.add('track--show');
    this._currentTrack = track;
    track.handlers.shift();
  },

  _hideTrack: function(track) {
    this.$.track.classList.remove('track--show');
    track.handlers = null;
  }

  /**
   * Fired when video start to play.
   * @event video-play-started
   */

});
