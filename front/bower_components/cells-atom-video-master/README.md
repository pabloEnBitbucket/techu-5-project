# cells-atom-video

[![Certificated](https://img.shields.io/badge/certificated-yes-brightgreen.svg)](http://bbva-files.s3.amazonaws.com/cells/bbva-catalog/index.html)

Wrapper for video tag.

Example:
```html
<cells-atom-video sources='[{"src": "video.mp4", "type": "mp4"}]' poster="poster.png"></cells-atom-video>
```

You can play a video as element background with `background` attribute:
```html
<cells-atom-video sources='[{"src": "video.mp4", "type": "mp4"}]' poster="poster.png" background></cells-atom-video>
```

You can play a video with different object-fit properties to get the video to be shown correctly in the desired screen. The attributes go as follows:

| Attribute                                | Description                                 |
|:----------------------------------------:|:-------------------------------------------:|
| fit-cover                                | applies object-fit: cover                   |
| fit-contain                              | applies object-fit: contain                 |
| fit-fill                                 | applies object-fit: fill                    |
| fit-scale-down                           | applies object-fit: scale-down              |
| fit-none                                 | applies object-fit: none                    |

Example:
```html
<cells-atom-video sources='[{"src": "video.mp4", "type": "mp4"}]' poster="poster.png" fit-cover></cells-atom-video>
```

## Styling

The following custom properties and mixins are available for styling:

| Custom property | Description     | Default        |
|:---------------:|:---------------:| :-------------:|
| --cells-atom-video  | empty mixin     | {}             |
| --cells-atom-video-video  | mixin for video tag    | {}             |
| --cells-atom-video-video-width  | width for video tag    | 100%             |
| --cells-atom-video-video-height  | height for video tag    | 100%             |
| --cells-atom-video-fit-cover  | mixin for video tag when fit-cover property applied    | {}             |
| --cells-atom-video-fit-fill  | mixin for video tag when fit-fill property applied    | {}             |
| --cells-atom-video-fit-contain  | mixin for video tag when fit-contain property applied    | {}             |
| --cells-atom-video-fit-scale-down  | mixin for video tag when fit-scale-down property applied    | {}             |
| --cells-atom-video-fit-none  | mixin for video tag when fit-none property applied    | {}             |
| --cells-atom-video-background-position  | video tag position when background property applied    | fixed             |
| --cells-atom-video-background  | mixin for video tag when background property applied    | fixed             |
| --cells-atom-video-track  | mixin for track    | {}             |
| --cells-atom-video-track-show  | mixin for track is showed    | {}             |
| --cells-atom-video-track-color  | color for track    | #fff           |
| --cells-atom-video-track-title  | mixin for track title    | {}           |
| --cells-atom-video-track-text  | mixin for track text    | {}           |
