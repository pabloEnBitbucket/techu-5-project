![cells-molecule-vimeo-player screenshot](cells-molecule-vimeo-player.jpg)
# cells-molecule-vimeo-player

[![Certificated](https://img.shields.io/badge/certificated-yes-brightgreen.svg)](http://bbva-files.s3.amazonaws.com/cells/bbva-catalog/index.html)

[Demo of component in Cells Catalog](http://bbva-files.s3.amazonaws.com/cells/bbva-catalog/index.html#/elements/cells-molecule-vimeo-player)

Player for VIMEO videos.

Example:
```html
<cells-molecule-vimeo-player  video-id="121748332"></cells-molecule-vimeo-player>
```

## Styling

The following custom properties and mixins are available for styling:

| Custom property | Description     | Default        |
|:---------------:|:---------------:| :-------------:|
| --cells-molecule-vimeo-player-iframe | empty mixin for iframe | {} |
| --cells-molecule-vimeo-player  | empty mixin     | {}             |
