(function() {

  'use strict';

  Polymer({

    is: 'cells-molecule-vimeo-player',

    properties: {
      /*
      * vimeo video id
      */
      videoId: {
        type: String,
        observer: '_composeUrl',
        value: '121748332'
      },
      /*
      * vimeo composed url
      */
      videoUrl: {
        type: String,
        value: ''
      },
      /*
      * stop video property
      */
      stopVideo: {
        type: String,
        notify: true,
        observer: '_stopVideo'
      }
    },

    _composeUrl: function() {
      this.set('videoUrl', 'https://player.vimeo.com/video/' + this.videoId + '?api=1&player_id=videoIFrame&color=ffffff&title=0&byline=0&portrait=0&badge=0');
    },

    _stopVideo: function() {
      if (this.stopVideo === 'true') {
        var player = this.$.videoIFrame;
        var playerOrigin = '*';
        var data = {
          method: 'pause'
        };
        var message = JSON.stringify(data);
        player.contentWindow.postMessage(message, playerOrigin);
      }

      this.set('stopVideo', 'false');
    }

  });
}());